# Crypto-API
App Stack:
- Python3
- Flask

### Base requirements and Installation
API requires Python3 Runtime and Flask Modules
Installing Python3
```sh
sudo apt update
sudo apt install python3 pip3
```

Installing Docker and Docker-Compose
```sh
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
rm -f get-docker.sh

sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

Run the API using Docker:
```sh
cd /path/to/code/directory
docker-compose up -d
```

Run the API using K8s:
```sh
cd /path/to/code/directory
kubectl apply -f crypto-api-k8s.yml
```

Encrypt Data:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"Input":"Some-Random-Data-Kaboom"}' \
  http://localhost:80/api/encrypt
```

Decrypt Data:
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"Input":"gAAAAABflI5lJi2qOOPgCOlO1Ae-MAvzGu_AGBlfJ3d10vbbmZ4fgYjvdSRn7QbRxNZ5ZKIh9o6CYtb-PT55LMB8d3CXinKP0tGQsclirg9-KGcU6HtfL8k="}' \
  http://localhost:80/api/decrypt
```

Get Health status:
```sh
curl --request GET http://localhost:80/api/health
```
